#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

/*
  MSmits 03:50PM
  what kind of math do you mean? Don't you consider various paths?
  Icebox 03:50PM

  I consider every single possible path, that's how it works
  MSmits 03:50PM
  i have a scoring system for all possible actions based on the distance to sites, but that distance is straight line distance, not path length
  i need the path length for it to be good

  Icebox 03:50PM
  Take your point
  take target site
  draw a line
  check intersections
  check which side is better to go around the circle
  to avoid collisions
*/

// L vs XecK
// seed=71105867
// W vs XecK
// seed=368679165
// L vs Inorry
// seed=876161114
// L vs Inorry
// seed=818201140

enum site_enum { SITE_ID, SITE_X, SITE_Y, RADIUS, GOLD, MAX_MINE, SITE_TYPE, SITE_OWNER, PARAM1, PARAM2 };
enum unit_enum { X, Y, UNIT_OWNER, UNIT_TYPE, HEALTH};
enum site_type { MINE, TOWER, BARRACKS };
enum quads { LEFT, CENTER, RIGHT };

#define M_PI 3.14159265358979323846
#define SITE_SIZE 10
#define UNIT_SIZE 5
#define X_SIZE 1920
#define Y_SIZE 1000
typedef int site[SITE_SIZE];
typedef int unit[UNIT_SIZE];
// contains a distance and a index (for sites)
typedef int didx[2];

int didx_cmp(const void *a, const void *b)
{
  return (((didx*)a)[0] < ((didx*)b)[0]) ? -1 : 1;
}

int dist(x1, y1, x2, y2)
{
  return fabs(sqrt((x1 - x2) * (x1 - x2))) + fabs(sqrt((y1 - y2) * (y1 - y2)));
}

int tdist(qx, qy, cx, cy, tx, ty)
{
  int dc = dist(cx, cy, tx, ty);
  int dq = dist(qx, qy, tx, ty);
  return (dc * .2) + dq;
}

// 3 quads, left, right, center
int center(x, y)
{
  return x > X_SIZE * .15 && x < X_SIZE *.85 &&
    y > Y_SIZE * .15 && y < Y_SIZE *.85;
}

void dp(char *name, int val)
{
  fprintf(stderr, "%s: %d\n", name, val);
}

void coll(qx, qy, tx, ty, ox, oy, dx, dy)
{
  // q is queen
  // t is target
  // o is obstacle
  // d is returned values

}

// more health
// push for early tower
// build archers once
// make towers bigger

// less health
// early knights?
// early towers?

// If you're not sure what to do, build a tower in the upper/lower corner farthest from your starting point
// build mines/towers until opp starts building giants?
// when being attacked, prefer building towers down towards corner

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
  int numSites;
  scanf("%d", &numSites);
  site sites[numSites];

  fprintf(stderr, "sites1: ===>%d\n", numSites);

  for (int i = 0; i < numSites; i++) {
    for (int j=0; j < SITE_SIZE; j++) sites[i][j] = 0;
  }

  for (int i = 0; i < numSites; i++) {
    for (int j=0; j < 4; j++) scanf("%d", &sites[i][j]);
  }

  int cx = -1, cy = -1;
  // site to go to
  int near = -1;
  int ctower = -1;

  // game loop
  while (1) {
    // set number of mines and barracks based on health?
    // build 2nd barracks after building towers and mines?
    // then double knight attack
    // wait to add more mines
    int num_mines = 2;
    int late_mines = 3;
    int gpt = 0;
    int num_towers = 3;
    int num_barracks = 1;

    int mines = 0, towers = 0, barracks = 0;
    int gold;
    int touchedSite; // -1 if none
    int ebarracks = 0;
    int etowers = 0;
    int etidxs[numSites];
    scanf("%d%d", &gold, &touchedSite);

    // scan sites
    for (int i = 0; i < numSites; i++) {
      int sid;
      scanf("%d", &sid);
      int s;
      for (int j = 0; j < numSites; j++) {
        if (sites[j][SITE_ID] == sid) s = j;
      }

      for (int j=4; j < SITE_SIZE; j++) scanf("%d", &sites[s][j]);
    }

    int numUnits;
    scanf("%d", &numUnits);
    unit units[numUnits];

    for (int i = 0; i < numUnits; i++) {
      for (int j=0; j < UNIT_SIZE; j++) scanf("%d", &units[i][j]);
    }

    // friendly queen unit
    int q = -1;
    for (int i=0; i < numUnits; i++) {
      if (units[i][UNIT_OWNER] == 0 && units[i][UNIT_TYPE] == -1) q = i;
    }

    if (cx == -1) {
      if (dist(units[q][X], units[q][Y], 0, 0) < dist(units[q][X], units[q][Y], 1920, 1000)) {
        cx = 0;
        cy = Y_SIZE;
      } else {
        cx = X_SIZE;
        cy = 0;
      }
    }

    if (ctower != -1) {
      int ctowerd = 100000;
      for (int i=0; i < numSites; i++) {
        int cd = dist(cx, cy, sites[i][SITE_X], sites[i][SITE_Y]);

        if (cd < ctowerd) { ctower = i; ctowerd = cd; }
      }
    }


    int creep = -1;
    for (int i=0; i < numUnits; i++) {
      if (units[i][UNIT_OWNER] != 1 || units[i][UNIT_TYPE] != 0) continue;
      int d = dist(units[q][X], units[q][Y], units[i][X], units[i][Y]);
      if (creep == -1 || d < creep) creep = d;
    }

    fprintf(stderr, "creep: %d", creep);

    didx didxs[numSites];
    for (int i =0; i < numSites; i++) {
      int d = dist(units[q][X], units[q][Y], sites[i][SITE_X], sites[i][SITE_Y]);
      didxs[i][0] = d;
      didxs[i][1] = i;
      if (sites[i][SITE_OWNER] == 1 && sites[i][SITE_TYPE] == TOWER) {
        etidxs[etowers] = i;
        etowers++;
      }
      if (sites[i][SITE_OWNER] == 0 && sites[i][SITE_TYPE] == BARRACKS) barracks++;
      if (sites[i][SITE_OWNER] == 1 && sites[i][SITE_TYPE] == BARRACKS) ebarracks++;
      if (sites[i][SITE_OWNER] == 0 && sites[i][SITE_TYPE] == MINE) {
        gpt += sites[i][PARAM1];
        mines++;
      }
      if (sites[i][SITE_OWNER] == 0 && sites[i][SITE_TYPE] == TOWER) towers++;
    }

    if (gpt >= 5) num_barracks = 2;

    // print sites
    /*
    for (int i=0; i < numSites; i++) {
      for(int j=0; j < SITE_SIZE; j++)
        fprintf(stderr, "%d ", sites[i][j]);
      fprintf(stderr, "\n");
    }
    */

    // reset near if we built something already
    if (sites[near][SITE_TYPE] != -1) near = -1;
    int mindist = -1;
    if (near == -1) {
      for (int i=0; i < numSites; i++) {
        int skip = 0;
        // ignore locations inside enemy tower range
        for (int j=0; j < etowers; j++) {
          int *s = sites[etidxs[j]];
          int rd = dist(sites[i][SITE_X], sites[i][SITE_Y], s[SITE_X], s[SITE_Y]);
          int ratk = s[PARAM2];
          if ( rd < ratk ) skip = 1;
          // fprintf(stderr, "sid: %d tid: %d rd: %d ratk: %d skip: %d\n", sites[i][SITE_ID], s[SITE_ID], rd, ratk, skip);
        }
        if (skip == 1) continue;
        // fprintf(stderr, "id: %d, gold: %d\n", sites[i][SITE_ID], sites[i][GOLD]);
        if (sites[i][SITE_X] > X_SIZE / 2 && cx == 0) continue;
        if (sites[i][SITE_X] < X_SIZE / 2 && cx == 1920) continue;
        if (sites[i][SITE_TYPE] == BARRACKS) continue;
        if (sites[i][MAX_MINE] > 0 && sites[i][MAX_MINE] == sites[i][PARAM1]) continue;
        if (sites[i][SITE_TYPE] == TOWER && sites[i][PARAM1] > 500) continue;
        int d = dist(units[q][X], units[q][Y], sites[i][SITE_X], sites[i][SITE_Y]);
        if (mindist == -1) { mindist = d; near = i; }

        fprintf(stderr, "id: %d d: %d g: %d mxg: %d\n", sites[i][SITE_ID], d, sites[i][GOLD], sites[i][MAX_MINE]);

        if (d < mindist) {
          mindist = d;
          near = i;
        }
      }
    }

    dp("gpt", gpt);
    dp("nb", num_barracks);

    //fprintf(stderr, "nearest: %d\n", near);

    // target site to train
    int train = -1;
    for (int i=0; i < numSites; i++ ) {
      if (sites[i][SITE_OWNER] == 0 && sites[i][SITE_TYPE] == BARRACKS && gold >= 80) train = i;
    }

    // fprintf(stderr, "idx type: %d\n", sites[near][SITE_TYPE]);
    // fprintf(stderr, "creep_dist: %d\n", creep);

    if (creep != -1 && creep < 600) {
      //      int close3[3] = {100000,100000,100000};
      near = -1;
      int mindist = -1;

      for (int i=0; i < numSites; i++) {
        int d = tdist(units[q][X], units[q][Y], cx, cy, sites[i][SITE_X], sites[i][SITE_Y]);
        if (sites[i][SITE_TYPE] == TOWER && sites[i][PARAM1] > 400) continue;
        if (mindist == -1 || d < mindist) {
          mindist = d;
          near = i;
        }
      }
    }

    // First line: A valid queen action
    // Second line: A set of training instructions
    if (near != -1) {

      if (creep != -1 && creep < 600) {
        printf("BUILD %d TOWER\n", sites[near][SITE_ID]);
      } else if (barracks == 0 && sites[near][SITE_TYPE] != MINE && sites[near][MAX_MINE] == 1) {
        printf("BUILD %d BARRACKS-KNIGHT\n", sites[near][SITE_ID]);
      } else if (sites[near][MAX_MINE] > 2 && sites[near][GOLD] != 0) {
        printf("BUILD %d MINE\n", sites[near][SITE_ID]);
      } else if (sites[near][GOLD] != 0 && (mines < num_mines || sites[near][SITE_TYPE] == MINE)) {
        printf("BUILD %d MINE\n", sites[near][SITE_ID]);
        //      } else if (creep < 525) {
        // printf("MOVE 0 0\n"); //BUILD %d TOWER\n", sites[near][SITE_ID]);
      } else if (center(sites[near][SITE_X], sites[near][SITE_Y]) && (towers < num_towers || sites[near][SITE_TYPE] == TOWER) && ebarracks > 0) {
        printf("BUILD %d TOWER\n", sites[near][SITE_ID]);
      } else if (barracks < num_barracks) {
        printf("BUILD %d BARRACKS-KNIGHT\n", sites[near][SITE_ID]);
      } else if (sites[near][GOLD] != 0 && mines < late_mines) {
        printf("BUILD %d MINE\n", sites[near][SITE_ID]);
      } else if (center(sites[near][SITE_X], sites[near][SITE_Y])) {
        printf("BUILD %d TOWER\n", sites[near][SITE_ID]);
      } else if (sites[near][GOLD] != 0) {
        printf("BUILD %d MINE\n", sites[near][SITE_ID]);
      } else {
        printf("BUILD %d TOWER\n", sites[ctower][SITE_ID]);
      }
    } else {
      printf("WAIT\n");
    }

    if (train != -1 && gold >= 80 * barracks) {
      char train[500] = "TRAIN";
      for (int i=0; i < numSites; i++) {
        if (sites[i][SITE_OWNER] == 0 && sites[i][SITE_TYPE] == BARRACKS) {
          char id[12];
          sprintf(id, " %d", sites[i][SITE_ID]);
          strcat(train, id);
        }
      }
      printf("%s\n", train);
    } else {
      printf("TRAIN\n");
    }
  }

  return 0;
}
