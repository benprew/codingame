import java.util.*;
import java.io.*;
import java.math.*;

class Sample {
    public int sampleId;
    public int carriedBy;
    public int rank;
    public String expertiseGain;
    public int health;
    public int costA, costB, costC, costD, costE;

    Sample(int sampleId,
           int carriedBy,
           int rank,
           String expertiseGain,
           int health,
           int costA,
           int costB,
           int costC,
           int costD,
           int costE) {
        this.sampleId = sampleId;
        this.carriedBy = carriedBy;
        this.rank = rank;
        this.expertiseGain = expertiseGain;
        this.health = health;
        this.costA = costA;
        this.costB = costB;
        this.costC = costC;
        this.costD = costD;
        this.costE = costE;
    }

    public String toString() {
        return
            sampleId + " " +
            carriedBy + " " +
            rank + " " +
            expertiseGain + " " +
            health + " " +
            costA + " " +
            costB + " " +
            costC + " " +
            costD + " " +
            costE;
    }
}

class Bot {
    public String target;
    public int eta;
    public int score;
    public int storageA;
    public int storageB;
    public int storageC;
    public int storageD;
    public int storageE;
    public int expertiseA;
    public int expertiseB;
    public int expertiseC;
    public int expertiseD;
    public int expertiseE;
    public Integer[] samples = new Integer[3];

    Bot(String target,
           int eta,
           int score,
           int storageA,
           int storageB,
           int storageC,
           int storageD,
           int storageE,
           int expertiseA,
           int expertiseB,
           int expertiseC,
           int expertiseD,
           int expertiseE) {
        this.target = target;
        this.eta = eta;
        this.score = score;
        this.storageA = storageA;
        this.storageB = storageB;
        this.storageC = storageC;
        this.storageD = storageD;
        this.storageE = storageE;
        this.expertiseA = expertiseA;
        this.expertiseB = expertiseB;
        this.expertiseC = expertiseC;
        this.expertiseD = expertiseD;
        this.expertiseE = expertiseE;
    }

    public String toString() {
        return
            target + " " +
            eta + " " +
            score + " " +
            storageA + " " +
            storageB + " " +
            storageC + " " +
            storageD + " " +
            storageE + " " +
            expertiseA + " " +
            expertiseB + " " +
            expertiseC + " " +
            expertiseD + " " +
            expertiseE;
    }
}

/**
 * Bring data on patient samples from the diagnosis machine to the
 * laboratory with enough molecules to produce medicine!
 **/
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int projectCount = in.nextInt();
        for (int i = 0; i < projectCount; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int c = in.nextInt();
            int d = in.nextInt();
            int e = in.nextInt();
        }

        Bot[] players = new Bot[2];

        // game loop
        while (true) {
            for (int i = 0; i < 2; i++) {
                String target = in.next();
                int eta = in.nextInt();
                int score = in.nextInt();
                int storageA = in.nextInt();
                int storageB = in.nextInt();
                int storageC = in.nextInt();
                int storageD = in.nextInt();
                int storageE = in.nextInt();
                int expertiseA = in.nextInt();
                int expertiseB = in.nextInt();
                int expertiseC = in.nextInt();
                int expertiseD = in.nextInt();
                int expertiseE = in.nextInt();

                players[i] = new Bot(target, eta, score, storageA,
                                        storageB, storageC, storageD, storageE,
                                        expertiseA, expertiseB, expertiseC,
                                        expertiseD, expertiseE);
            }
            int availableA = in.nextInt();
            int availableB = in.nextInt();
            int availableC = in.nextInt();
            int availableD = in.nextInt();
            int availableE = in.nextInt();
            int sampleCount = in.nextInt();
            Sample[] samples = new Sample[sampleCount];
            for (int i = 0; i < sampleCount; i++) {
                int sampleId = in.nextInt();
                int carriedBy = in.nextInt();
                int rank = in.nextInt();
                String expertiseGain = in.next();
                int health = in.nextInt();
                int costA = in.nextInt();
                int costB = in.nextInt();
                int costC = in.nextInt();
                int costD = in.nextInt();
                int costE = in.nextInt();
                samples[i] = new Sample(sampleId, carriedBy, rank, expertiseGain, health, costA, costB, costC, costD, costE);
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");

            String DIAG = "DIAGNOSIS";
            String MOL = "MOLECULES";
            String LAB = "LABORATORY";

            for(int i = 0; i < samples.length; i++) {
                System.err.println(samples[i]);
            }

            Sample[] mySamples = Arrays.stream(samples).filter(s -> s.carriedBy == 0).toArray(Sample[]::new);
            Sample[] openSamples = Arrays.stream(samples).filter(s -> s.carriedBy == -1).toArray(Sample[]::new);

            if (players[0].eta != 0) {
                System.out.println("WAIT");
                continue;
            }

            if (players[0].target.equals("START_POS")) {
                System.out.println("GOTO " + DIAG);
                continue;
            }

            if (players[0].target.equals(DIAG)) {
                if (mySamples.length < 3) {
                    System.out.println("CONNECT " + openSamples[0].sampleId);
                } else {
                    System.out.println("GOTO " + MOL);
                }
            } else if (players[0].target.equals(MOL)) {
                int required = 0;
                if (required == 0) {
                    System.out.println("GOTO " + LAB);
                } else {
                    System.out.println("CONNECT A");
                }
            } else if (players[0].target.equals(LAB)) {
                System.out.println("CONNECT " + mySamples[0].sampleId);
            } else if (mySamples.length < 3) {
                if (players[0].target.equals(DIAG)) {

                } else {
                    System.out.println("GOTO " + DIAG);
                }
            } else {
                System.err.println(players[0]);
                System.err.println(samples);
            }
        }
    }
}
