#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_NODES 200000
struct node {
  int neighbors[100];
  int child_count;
};

struct node tree[MAX_NODES] = {{{0}, 0}};

void add_node(struct node *tree, int p, int c)
{
  tree[p].child_count++;
  for (int i = 0; i < 100; i++) {
    if (tree[p].neighbors[i] == -1) {
      tree[p].neighbors[i] = c;
      break;
    }
  }

  tree[c].child_count++;
  for (int i = 0; i < 100; i++) {
    if (tree[c].neighbors[i] == -1) {
      tree[c].neighbors[i] = p;
      break;
    }
  }
}


// QUEUE
struct queue_item {
  int node_id;
  struct queue_item *next;
  struct queue_item *prev;
};

struct queue_item *Front;
struct queue_item *Rear;

int dequeue()
{
  if (Front == NULL)
    return -1;

  int node_id = Rear->node_id;
  struct queue_item *tmp = Rear;

  if (Rear->prev == NULL)
    Front = Rear = NULL;
  else {
    Rear = Rear->prev;
    Rear->next = NULL;
  }

  free(tmp);

  return node_id;
}

void enqueue(int node_id)
{
  struct queue_item *top = (struct queue_item*)malloc(sizeof(struct queue_item));
  top->node_id = node_id;
  top->next = NULL;
  top->prev = NULL;

  if (Front != NULL) {
    Front->prev = top;
    top->next = Front;
  }
  Front = top;
  if (Rear == NULL) Rear = top;
}
// END QUEUE //

void print_tree(struct node *tree)
{
  return;
  for (int i = 0; i < MAX_NODES; i++)
    if (tree[i].child_count != 0)
      fprintf(stderr, "TREE id: %d child_count: %d n: %d\n", i, tree[i].child_count, tree[i].neighbors[0]);
}

int main()
{
  for (int i = 0; i < MAX_NODES; i++)
    for (int j = 0; j < 100; j++)
      tree[i].neighbors[j] = -1;

  int n; // the number of adjacency relations
  scanf("%d", &n);
  for (int i = 0; i < n; i++) {
    int p; // the ID of a person which is adjacent to yi
    int c; // the ID of a person which is adjacent to xi
    scanf("%d%d", &p, &c);

    add_node(tree, p, c);
    print_tree(tree);
  }

  int depth = 0;
  int added_item = 0;

  do {
    added_item = 0;
    for (int i = 0; i < MAX_NODES; i++) {
      if (tree[i].child_count == 1) {
        added_item = 1;
        enqueue(i);
      }
    }

    int node_id;

    while ((node_id = dequeue()) != -1) {
      struct node e = tree[node_id];


      tree[node_id].child_count--;
      int i = 0;
      int n;
      while ((n = e.neighbors[i]) != -1) {
        tree[n].child_count--;
        i++;
      }
    }

    if (added_item != 0) depth++;
  } while (added_item);

  printf("%d\n", depth);

  return 0;
}
