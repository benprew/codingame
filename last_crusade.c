#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// The room type, then the entrance, with the value being the exit direction
// Type 0 - 0 entrance, 0 exit
// Type 1 - 3 entrances (Left, Up, Right), 1 exit (Down)
// Type 2 - 1 entrace (Left), 1 exit (Right), 1 entrance (Right), 1 exit (Left)
// Type 3 - 1 entrance (Up), 1 exit (Down)
// Type 4 - 1 entrance (Up), 1 exit (Left), 1 entrance (Right), 1 exit (Down)
// Type 5 - 1 entrance (Up), 1 exit (Right), 1 entrance (Left), 1 exit (Down)
// Type 6 - 1 entrance (Left), 1 exit (Right), 1 entrance (Right), 1 exit (Left)
// Type 7 - 2 entrances (Up, Right), 1 exit (Down)
// Type 8 - 2 entrances (Left, Right), 1 exit (Down)
// Type 9 - 2 entrances (Up, Left), 1 exit (Down)
// Type 10 - 1 entrance (Up), 1 exit (Left)
// Type 11 - 1 entrance (Up), 1 exit (Right)
// Type 12 - 1 entrance (Right), 1 exit (Down)
// Type 13 - 1 entrance (Left), 1 exit (Down)


int main()
{
  char dirs[] = " D D   DDDLRDD";
  int W; // number of columns.
  int H; // number of rows.
  scanf("%d%d", &W, &H); fgetc(stdin);

  int map[H][W];
  for (int i = 0; i < H; i++)
    for (int j = 0; j < W ; j++)
      scanf("%d", &map[i][j]);

  int EX; scanf("%d", &EX);

  // start going down
  char d = 'D';
  // game loop
  while (1) {
    int XI;
    int YI;
    char POS[6];
    scanf("%d%d%s", &XI, &YI, POS);

    int room_type = map[YI][XI];

    // do nothing for left/right room
    if (room_type == 2 || room_type == 6) ;
    else if (room_type == 4) d = d == 'D' ? 'L' : 'D';
    else if (room_type == 5) d = d == 'D' ? 'R' : 'D';
    else d = dirs[room_type];

    if (d == 'D') printf("%d %d\n", XI, ++YI);
    else if (d == 'L') printf("%d %d\n", --XI, YI);
    else if (d == 'R') printf("%d %d\n", ++XI, YI);
    else printf("Uknown direction: %c\n", d);
  }
  return 0;
}
