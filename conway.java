import java.util.*;
import java.io.*;
import java.math.*;
import java.lang.String;

class Solution {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int R = in.nextInt();
        int L = in.nextInt();
        String line = Integer.toString(R);

        for(int i = 1; i < L; i++){
            line = next_line(line.split(" "));
        }

        System.out.println(line);
    }

    static String next_line(String[] parts) {
        String prev = parts[0];
        String last = parts[parts.length -1];
        ArrayList<String> outparts = new ArrayList<String>();
        int count = 0;
        for(String part: parts) {
            if (prev.equals(part)) {
                count++;
            } else if (count > 0) {
                outparts.add(Integer.toString(count) + " " + prev);
                count = 1;
            }
            prev = part;
        }

        if (count > 0)
            outparts.add(Integer.toString(count) + " " + last);

        String[] outStringArr = new String[outparts.size()];
        return String.join(" ", outparts.toArray(outStringArr));
    }
}
