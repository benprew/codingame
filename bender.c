#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

struct point {
  int x;
  int y;
};

char override_dir = '\0';
char dirs[5] = "SENW";
bool breakered = false;
bool exited = false;
struct point teleports[2];
// benders location
struct point b;

bool can_go(char **map, int x, int y)
{
  switch (map[y][x])
    {
    case '#':
      return false;
      break;

    case 'X':
      if (breakered) {
        map[y][x] = ' ';
        return true;
      }
      return false;
      break;
    }

  return true;
}

bool try_go(char **map, struct point *b_loc, char dir)
{
  int x = b_loc->x, y = b_loc->y;
  switch (dir)
    {
    case 'N':
      y--;
      break;
    case 'E':
      x++;
      break;
    case 'S':
      y++;
      break;
    case 'W':
      x--;
      break;
    }

  if (can_go(map, x, y)) {
    b_loc->x = x;
    b_loc->y = y;
    return true;
  }
  return false;
}

char next_dir(char **map, struct point *b_loc)
{
  if (override_dir != '\0')
    if (try_go(map, b_loc, override_dir))
      return override_dir;

  for (int i=0; i < 3; i++)
    if (try_go(map, b_loc, dirs[i]))
      return dirs[i];

  return '\0';
}

void teleport(struct point *b)
{
  bool tnum = false;
  struct point t = teleports[tnum];
  if(t.x == b->x && t.y == b->y)
    tnum = !tnum;

  b->x = teleports[tnum].x;
  b->y = teleports[tnum].y;
}

char move(char **map, struct point *b)
{
  if (exited) return '$';
  char dir = next_dir(map, b);
  char sq = map[b->y][b->x];

  override_dir = dir;

  if (sq == 'I') {
    if (dirs[0] == 'S')
      strcpy(dirs, "WNES");
    else
      strcpy(dirs, "SENW");
  }

  if (sq == '$')          exited = true;
  if (strchr("NESW", sq)) override_dir = sq;
  if (sq == 'B')          breakered = !breakered;
  if (sq == 'T')          teleport(b);

  return dir;
}

void p_map(char **map, int L, int x, int y)
{
  for (int l=0; l < L; l++) {
    int j = 0;
    while(map[l][j] != '\0') {
      if (l == y && j == x)
        fprintf(stderr, "@");
      else
        fprintf(stderr, "%c", map[l][j]);
      j++;
    }
  }
}

void setup(char **map, struct point *b, int L, int C)
{
  int tnum = 0;
  for (int l = 0; l < L; l++)
    for (int c=0; c < C; c++) {
      if (map[l][c] == '@') {
        b->x = c;
        b->y = l;
      }

      if (map[l][c] == 'T') {
        teleports[tnum].x = c;
        teleports[tnum].y = l;
        tnum++;
      }
    }
}

int main()
{
    int L;
    int C;
    char route[400000];

    scanf("%d%d", &L, &C); fgetc(stdin);

    char **map = malloc(sizeof(*map) * L);
    // for LOOP detection
    // Is like chess stalemate, if bender is in same position more then 10 times.
    int **b_locs = malloc(sizeof(*b_locs) * L);

    for (int l = 0; l < L; l++) {
      map[l] = malloc(sizeof(map) * (C+3));
      b_locs[l] = malloc(sizeof(b_locs) * C);
      fgets(map[l], C+2, stdin); // +2 for cr-nl
    }

    for (int i=0; i < L; i++)
      for (int j=0; j < C; j++)
        b_locs[i][j] = 0;


    setup(map, &b, L, C);

    do {
      p_map(map, L, b.x, b.y);
      b_locs[b.x][b.y]++;

      if (b_locs[b.x][b.y] > 10) {
        puts("LOOP");
        return 0;
      }
      char dir = move(map, &b);

      switch (dir)
        {
        case 'N':
          strcat(route, "NORTH\n");
          break;
        case 'E':
          strcat(route, "EAST\n");
          break;
        case 'S':
          strcat(route, "SOUTH\n");
          break;
        case 'W':
          strcat(route, "WEST\n");
          break;
        case '$':
          return 0;
          break;
        }
    } while (!exited);

    puts(route);

    return 0;
}
