import sys
import math


# local referee
# https://github.com/CodinGame/LegendsOfCodeAndMagic/

# list of cards:
# ...

ID=1
LOCATION=2
TYPE=3
COST=4
ATK=5
DEF=6
ABILITY=7

CREATURE=0
GREEN=1
RED=2
BLUE=3

MY_HAND=0
MY_BOARD=1
OP_BOARD=-1

def chg(total, sub):
    retrun (total - sub) / total

def score(card, target, op_health, op_atk, my_health, my_atk):
    if target[ATK]:
        return chg(my_atk, card[ATK]) - chg(op_atk, target[ATK])
    else:
        if 'G' in target[ABILITY]:
            return chg(my_health, target[ATK])
        else:
            return chg(my_health, target[ATK]) - chg(op_health, card[ATK])

def best_target(card, targets):
    score = 0
    best_target = None

    for target in targets:
        if card[ATK] < target[DEF]: continue
        tscore = score(card, target, )
        if tscore > score:
            score = tscore
            best_target = target
    return best_target

def targets(cards):
    op_cards = [c for c in cards if c[LOCATION] == OP_BOARD]
    targets = []
    # get guard cards
    for c in op_cards:
        if card[LOCATION] != OP_BOARD: continue
        if 'G' in card[ABILITY]: targets.append(card)

    if targets: return targets

    # return -1 for opponent
    return op_cards + [[0, -1]]

def my_atk(cards):
    return sum([c[ATK] for c in cards])

def op_atk(cards):
    return sum([c[ATK] for c in cards])


try_int = lambda x: int(x) if x.isdigit() else x
# game loop
while True:
    cards = []
    for i in range(2):
        player_health, player_mana, player_deck, player_rune = [int(j) for j in input().split()]
    opponent_hand = int(input())
    card_count = int(input())
    for i in range(card_count):
        cards.append(list(map(try_int, input().split())))
# card_number, instance_id, location, card_type, cost, attack, defense, abilities, my_health_change, opponent_health_change, card_draw = input().split()
# card_number = int(card_number)
# instance_id = int(instance_id)
# location = int(location)
# card_type = int(card_type)
# cost = int(cost)
# attack = int(attack)
# defense = int(defense)
# my_health_change = int(my_health_change)
# opponent_health_change = int(opponent_health_change)
# card_draw = int(card_draw)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)
    draft_id = 0
    for i, card in enumerate(cards):
        if card[TYPE] == CREATURE and card[ATK] > 1:
            draft_id = i
            break
    if player_mana == 0 and len(cards) == 3:
        print("PICK %d" % draft_id)
        continue


    my_board_cards = []
    op_guard_card_id = '-1'
    summon_card = None
    summonable = []
    commands = []

    for card in cards:
        if card[LOCATION] == MY_BOARD: my_board_cards.append(card)
        if card[LOCATION] == MY_HAND and card[COST] <= player_mana and card[TYPE] == CREATURE:
            summonable.append(card)

        if card[LOCATION] == OP_BOARD and 'G' in card[ABILITY]:
            op_guard_card_id = card[ID]

    my_board_cards.sort(key=lambda x: x[ATK])
    for card in my_board_cards:
        t = best_target(card, targets(cards))
        if t:
            commands.append("ATTACK %s %s" % (card[ID], t[ID]))
            # update cards

        if card[ATK] > 0:
            commands.append("ATTACK %s %s" % (card[ID], op_guard_card_id))

    if summonable:
        summonable.sort(key=lambda x: x[COST] * -1)
        summon_card = summonable[0]
        remaining_mana = player_mana
        for card in summonable:
            if card[COST] <= remaining_mana:
                commands.append("SUMMON %s" % summon_card[ID])
                remaining_mana -= card[COST]

    if commands: print(";".join(commands))
    else: print("PASS")
