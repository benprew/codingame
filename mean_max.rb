STDOUT.sync = true

# https://en.wikipedia.org/wiki/Minimax
# https://tech.io/playgrounds/334/genetic-algorithms/algorithm


def dist(a, b)
  Math.sqrt((b[:x] - a[:x])**2 + (b[:y] - a[:y])**2).abs
end

# TODO: target wreck that is farthest from enemy but closest to us?
def best_wreck(me, wrecks, reapers)
  # warn "before: #{wrecks.count}"

  # reject any wrecks that will be consumed by the enemy next turn
  wrecks = wrecks.reject do |w|
    w[:extra] == 1 &&
      dist(me, w) > 400 &&
      reapers.find do |r|
        dist(r, w) < 1000 && dist(r, w) < dist(me, w)
      end
  end

  # warn "after: #{wrecks.count}"
  return nil if wrecks.length.zero?
  return wrecks[0] if wrecks.length == 1

  d2 = wrecks.map { |w| [w[:id], dist(w, me)] }
  dists = d2.map { |d| d[1] }
  dist_scale = (dists.max - dists.min).to_f
  dist_avg = dists.reduce(:+) / dists.count.to_f

  e2 = wrecks.map do |w|
    extras = wrecks.select { |w2| dist(w, w2) <= 1000 }
    [w[:id], extras.map { |e| e[:extra] }.reduce(:+)]
  end
  extras = e2.map { |e| e[1] }
  extra_scale = (extras.max - extras.min).to_f
  extra_scale = 1 if extra_scale == 0
  extra_avg = extras.reduce(:+) / extras.count.to_f

  scored = wrecks.map do |w|
    dscore = (dist(me, w) - dist_avg) / dist_scale
    escore = -((w[:extra] - extra_avg) / extra_scale)
    score = dscore + escore
    # warn "#{dscore} #{dist_avg} #{dist_scale} #{escore} #{extra_avg} #{extra_scale}"
    # warn "#{w[:id]} s: #{score} d: #{dist(me, w)}"
    [score * 100, w]
  end

  best = scored.min_by { |a| a[0] }.last
  warn "best_wreck: #{best[:id]} #{best[:x]} #{best[:y]}"
  best
end

# Find the best target for the destroyer
# Heuristics:
# - distance between destroyer and reaper
# - TODO: don't blow up tankers that an enemy reaper is closer to
def best_tanker(reap, dest, tankers, op_reapers)
  tankers.select { |t| t[:extra] > 1 }.min_by do |t|
    op_sum_dist = (6000 - op_reapers.map { |o| dist(t, o) }.reduce(:+))
    sum_dist = dist(t, reap) + dist(t, dest) + op_sum_dist
    # warn "bst_tnkr: #{t[:id]} #{sum_dist} #{op_sum_dist}"
    sum_dist
  end
end

# Heuristics:
# - keep an opponent from harvesting water
def oil_target(my_reaper, doof, op_reapers, wrecks)
  opps = op_reapers.select do |r|
    dist(doof, r) <= 2000 &&
      wrecks.select { |w| dist(r, w) <= 400 }.count > 0 &&
      dist(r, my_reaper) >= 1000
  end

  warn "rage: #{doof[:rage]} opps: #{opps.count}"

  return nil unless doof[:rage] >= 60 && opps.count > 0

  opps.max_by { |a| a[:score] }
end

# throw a grenade that will:
# Heuristics:
# - push an opponent past a wreck - target just behind opp. reaper
# - push an opponent "blocking" our reaper - target reaper
def grenade(unit, reap, opps, wrecks)
  return nil unless reap[:rage] >= 60
  warn "rage: #{reap[:rage]} dist: #{dist(unit, reap)}"

  opps.find { |o| dist(o, reap) < 1000 } &&
    wrecks.find { |w| dist(w, reap) < 1000 } &&
    reap
end

def best_doof_target(reaps)
  reaps.reject { |r| r[:player] == 0 }.max_by { |a| a[:score] }
end

def steer(reaper, point)
    # From https://tech.io/playgrounds/1003/flocking-autonomous-agents/steering-strategy
    point[:x] -= reaper[:vx]
    point[:y] -= reaper[:vy]

    point
end

# me - my reaper
# units - other tankers
# target - location we're trying to get to
# need to calculate where we will be next turn, so we can find any obstacles near us
# from https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-collision-avoidance--gamedev-7777
def avoid(me, target, units)
  return target
  ahead = {
    x: me[:x] + me[:vx] < 0 ? me[:vx] - 300 : me[:vx] + 300,
    y: me[:y] + me[:vy] < 0 ? me[:vy] - 300 : me[:vy] + 300
  }

  warn "tgt: #{target[:x]} #{target[:y]}"
  warn "ahead: #{ahead[:x]} #{ahead[:y]}"

  rad_adj = 1

  threat = units
           .map { |u| return u; rad = u[:radius] * rad_adj; warn "u: #{u[:id]} d:#{dist(ahead, u).to_i} rad: #{rad}"; u }
           .select { |u| dist(ahead, u) <= u[:radius] * rad_adj } # units that we hit next turn
           .min_by { |u| dist(me, u) } # pick the closest

  return target unless threat

  warn "tgt1: #{target[:x]} #{target[:y]}"
  target[:x] -= threat[:x]
  target[:y] -= threat[:y]
  warn "tgt2: #{target[:x]} #{target[:y]}"

  target
end

# game loop
loop do
  units = {}
  scores = (0..2).map { |i| gets.to_i }
  rages = (0..2).map { |i| gets.to_i }
  unit_count = gets.to_i
  unit_count.times do
    line = gets
    # warn line
    unit_id, unit_type, player, mass, radius, x, y, vx, vy, extra, extra_2 = line.split(" ")
    unit_id = unit_id.to_i
    unit_type = unit_type.to_i
    player = player.to_i
    mass = mass.to_f
    radius = radius.to_i
    x = x.to_i
    y = y.to_i
    vx = vx.to_i
    vy = vy.to_i
    extra = extra.to_i
    extra_2 = extra_2.to_i
    units[unit_id] = {
      id: unit_id,
      type: unit_type,
      player: player,
      score: player == -1 ? nil : scores[player],
      rage: player == -1 ? nil : rages[player],
      mass: mass,
      radius: radius,
      x: x,
      y: y,
      vx: vx,
      vy: vy,
      extra: extra,
      extra2: extra_2
    }
  end

  reaper = units.values.find { |u| u[:player] == 0 && u[:type] == 0 }
  destroyer = units.values.find { |u| u[:player] == 0 && u[:type] == 1 }
  doof = units.values.find { |u| u[:player] == 0 && u[:type] == 2 }
  op_reapers = units.values.select { |u| u[:type] == 0 && u[:player] != 0 }
  tankers = units.values.select { |v| v[:type] == 3 }
  wrecks = units.values.select { |v| v[:type] == 4 }
  opps = units.values.select { |u| [1, 2].include?(u[:player]) }

  if (rpoint = best_wreck(reaper, wrecks, op_reapers))
    rpoint = steer(reaper, rpoint)
    rpoint = avoid(reaper, rpoint, tankers)# units.values.reject { |u| u[:id] == reaper[:id] })
    puts "#{rpoint[:x]} #{rpoint[:y]} 300 REBOL"
  else
    puts '0 0 200'
    # puts "#{destroyer[:x]} #{destroyer[:y]} 200"
  end

  if (point = grenade(destroyer, reaper, tankers + opps, wrecks))
    puts "SKILL #{point[:x]} #{point[:y]} TBOMB"
  elsif (dpoint = best_tanker(reaper, destroyer, tankers, op_reapers))
    x = dpoint[:x] #- destroyer[:vx]
    y = dpoint[:y] #- destroyer[:vy]
    puts "#{x} #{y} 300 #{dpoint[:id]}"
  else
    puts '0 0 100'
  end

  d = best_doof_target(op_reapers)
  if (t = oil_target(reaper, doof, op_reapers, wrecks))
    puts "SKILL #{t[:x]} #{t[:y]} DBOMB"
  else
    puts "#{d[:x]} #{d[:y]} 300"
  end
end
