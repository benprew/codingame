STDOUT.sync = true # DO NOT REMOVE
# Save humans, destroy zombies!

humans = []

# game loop
loop do
    _ax, _ay = gets.split(' ').map(&:to_i)
    human_count = gets.to_i
    human_count.times do
      id, x, y = gets.split(' ').map(&:to_i)
      humans[id] = [x, y]
    end
    zombies = []
    zombie_count = gets.to_i
    zombie_count.times do
      id, x, y, xn, yn = gets.split(' ').map(&:to_i)
      warn id, x, y, xn, yn
      zombies << [x, y, xn, yn]
    end

    if zombie_count == 1
      puts "#{zombies[0][0]} #{zombies[0][1]}"
    else
      puts "#{humans[0][0]} #{humans[0][1]}"
    end
end

# opportunities to kill zombies
def opps(zombies)
  zombies.each do |z|
    xn, yn = z[2..3]
  end
end
