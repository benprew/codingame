use std::io;

macro_rules! pi {
    ($x:expr, $t:ident) => ($x.trim().parse::<$t>().unwrap())
}

fn main() {
    let mut elevs: [i32; 500] = [0; 500];
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let inputs = input_line.split(" ").collect::<Vec<_>>();

    let nb_elevators = pi!(inputs[7], i32);
    for _i in 0..nb_elevators as usize {
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let inputs = input_line.split(" ").collect::<Vec<_>>();
        let elevator_floor = pi!(inputs[0], i32);
        let elevator_pos = pi!(inputs[1], i32);
        elevs[elevator_floor as usize] = elevator_pos;
    }
    elevs[pi!(inputs[3], i32) as usize] = pi!(inputs[4], i32);
    loop {
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let inputs = input_line.split(" ").collect::<Vec<_>>();
        let clone_floor = pi!(inputs[0], i32);
        let clone_pos = pi!(inputs[1], i32);
        let direction = inputs[2].trim().to_string();

        if clone_floor == -1 {
            println!("WAIT");
            continue;
        }
        let floor = clone_floor as usize;
        match direction.as_ref() {
            "LEFT" if clone_pos - elevs[floor] < 0 => println!("BLOCK"),
            "RIGHT" if clone_pos - elevs[floor] > 0 => println!("BLOCK"),
            _ => println!("WAIT"),
        };
    }
}
